n = int(input("Enter n: "))
max_width = (2*n - 1) * 2

for i in range(1, n+1):
    line = " ".join(str(x) for x in range(1, i+1))
    line = line + " " + " ".join(str(x) for x in range(i-1, 0, -1))
    line = line.center(max_width).rstrip()
    print(line)
