min_width = int(input("Enter the minimum width: "))
max_width = int(input("Enter the maximum width: "))

if min_width > max_width:
    print("Error: minimum width is greater than maximum width!")
    quit()

if (max_width - min_width) % 2 != 0:
    print("Error: the difference between maximum and minimum width is not divisible by 2!")
    quit()

for i in range(min_width, max_width+1, 2):
    if i == min_width:
        diamond = "*" * i
    else:
        diamond = "*" + " "*(i-2) + "*"
    print("{:^{width}}".format(diamond, width=max_width).rstrip())

for i in range(max_width-2, min_width-1, -2):
    if i == min_width:
        diamond = "*" * i
    else:
        diamond = "*" + " "*(i-2) + "*"
    print("{:^{width}}".format(diamond, width=max_width).rstrip())
